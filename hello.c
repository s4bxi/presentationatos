/*
 * Author: Julien EMMANUEL
 * Copyright (C) 2019-2021 Bull S.A.S
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation,
 * which comes with this package.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <portals4.h>
#include <portals4_bxiext.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#define MESSAGE_NUMBER 10
#define PID 2567
#define BUFFER_SIZE 1024000

int client(char* target)
{
    struct timeval start, end;

    PtlInit();
    ptl_handle_ni_t nih;
    int rc = PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PID, NULL, NULL, &nih);

    ptl_md_t mdpar;
    ptl_handle_eq_t eqh;
    ptl_handle_md_t mdh;
    ptl_process_t peer;
    ptl_event_t ev;

    rc = PtlEQAlloc(nih, 256, &eqh);

    peer.phys.nid = atoi(target);
    peer.phys.pid = PID;

    int buffer_size = BUFFER_SIZE;
    double comm_time, total_size_kB;

    char* buf = (char*)malloc(buffer_size * sizeof(char));

    mdpar.start     = buf;
    mdpar.length    = buffer_size;
    mdpar.eq_handle = eqh;
    mdpar.ct_handle = PTL_CT_NONE;
    mdpar.options   = PTL_MD_EVENT_SEND_DISABLE | PTL_MD_VOLATILE;

    rc = PtlMDBind(nih, &mdpar, &mdh);

    gettimeofday(&start, NULL);

    for (int j = 0; j < MESSAGE_NUMBER; ++j) {
	sprintf(buf, "Message n°%d\n", j); 
        // Should match with 3rd ME (40 + 2)
        rc = PtlPut(mdh, 0, buffer_size, PTL_ACK_REQ, peer, 0, 42, 0, NULL, 100);

        PtlEQWait(eqh, &ev);
    }

    total_size_kB = (float)buffer_size * MESSAGE_NUMBER / 1024.0F;
    gettimeofday(&end, NULL);
    comm_time = (end.tv_sec * 1e6 + end.tv_usec - (start.tv_sec * 1e6 + start.tv_usec));
    printf("Client transferred %ldkB in %fs (%fMB/s)\n", (long)total_size_kB, comm_time, (total_size_kB / 1024.0F) / (comm_time / 1e6));

    rc = PtlMDRelease(mdh);

    free(buf);

    rc = PtlEQFree(eqh);

    rc = PtlNIFini(nih);
    PtlFini();

    return 0;
}

int server()
{
    ptl_handle_ni_t nih;
    int rc = PtlInit();

    PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PID, NULL, NULL, &nih);

    ptl_handle_eq_t eqh;
    char* buf;
    ptl_me_t mepar;
    ptl_handle_me_t meh_pool;

    rc = PtlEQAlloc(nih, 256, &eqh);

    ptl_pt_index_t pte;

    rc = PtlPTAlloc(nih, 0, eqh, 0, &pte);
    ptl_event_t ev;

    int i, j;

    buf = (char*)malloc(BUFFER_SIZE * sizeof(char));
    memset(&mepar, 0, sizeof(ptl_me_t));
    mepar.start       = buf;
    mepar.length      = BUFFER_SIZE;
    mepar.ct_handle   = PTL_CT_NONE;
    mepar.match_bits  = 42;
    mepar.ignore_bits = 0;
    mepar.uid         = PTL_UID_ANY;
    mepar.options     = PTL_LE_OP_PUT;

    rc = PtlMEAppend(nih, pte, &mepar, PTL_PRIORITY_LIST, meh_pool, &meh_pool);
    rc = PtlEQWait(eqh, &ev);
    if (ev.type != PTL_EVENT_LINK) {
        fprintf(stderr, "Wrong event type, got %u instead of LINK (%u)", ev.type, PTL_EVENT_LINK);
        exit(1);
    }
    
    unsigned int which;

    for (j = 0; j < MESSAGE_NUMBER; ++j) {
        for (;;) {
            rc = PtlEQPoll(&eqh, 1, 5000, &ev, &which);
            if (rc == PTL_OK)
                break;
        }

        if (ev.type != PTL_EVENT_PUT) {
            fprintf(stderr, "Wrong event type, got %u instead of PUT (%u)", ev.type, PTL_EVENT_PUT);
            exit(1);
        } else {
	    printf("Server received: %s", buf); 
	}
    }

    free(buf);

    PtlMEUnlink(meh_pool);
    PtlPTFree(nih, pte);
    PtlEQFree(eqh);
    PtlNIFini(nih);
    PtlFini();

    return 0;
}

int main(int argc, char* argv[])
{
    // the client has a parameter (who the server is)
    return argc > 1 ? client(argv[1]) : server();
}
