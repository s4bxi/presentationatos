#!/bin/bash

mkdir -p build

# Compile platform
g++ amour.cpp -o ./build/libamour.so -shared -I/opt/simgrid/include -fPIC

# Compile user code
s4bxicc ./hello.c -o build/hello
# equivalent to `gcc -shared -I/opt/s4bxi/include -fPIC ./hello.c -o build/hello`

# Run simulation
s4bximain ./build/libamour.so ./deploy.xml ./build/hello hello
# optionnally use `mkdir logs && S4BXI_LOG_FOLDER=logs` at the start to log all operations

