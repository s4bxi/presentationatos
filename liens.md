## SimGrid

**Repo principal :**

https://framagit.org/simgrid/simgrid

**Mon script de build :**

https://framagit.org/Webcretaire/simgrid/-/blob/s4bxi/s4bxi_build.sh

## S4BXI

**Repo :**

https://framagit.org/s4bxi/s4bxi

## Génération de déploiement

**Repo :**

https://framagit.org/s4bxi/s4bxi-config-generator

**Binaires pour Linux (aucune dépendance) :**

https://framagit.org/s4bxi/s4bxi-config-generator/-/jobs/1469464/artifacts/browse

