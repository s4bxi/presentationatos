/*
 * Author: Julien EMMANUEL
 * Copyright (C) 2020-2022 Bull S.A.S
 * All rights reserved
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation,
 * which comes with this package.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "simgrid/s4u.hpp"

using namespace simgrid;

constexpr int num_cpus = 64;
constexpr double cpu_speed = 10e9;
constexpr double nic_speed = 1e9;
constexpr double pci_bw = 15.75e9;
constexpr double bxi_bw = 11.1e9;
constexpr double pci_lat = 250e-9;
constexpr double bxi_lat = 250e-9;

static std::pair<simgrid::kernel::routing::NetPoint *, simgrid::kernel::routing::NetPoint *>
create_hostzone(const s4u::NetZone *zone, const std::vector<unsigned long> & /*coord*/, unsigned long id)
{
    std::string hostname = "amour" + std::to_string(id + 1);

    // create the zone
    auto *host_zone = s4u::create_floyd_zone(hostname);
    host_zone->set_parent(zone);

    // create NIC
    std::string nic_name = hostname + "_NIC";
    const s4u::Host *nic = host_zone->create_host(nic_name, nic_speed)->seal();

    // create PCI cable
    auto pciNotFat = host_zone->create_split_duplex_link(hostname + "_NIC_PCI", pci_bw)->set_latency(pci_lat)->seal();
    auto pciFat = host_zone->create_link(hostname + "_NIC_PCI_FAT", bxi_bw)
                      ->set_sharing_policy(s4u::Link::SharingPolicy::FATPIPE)
                      ->set_latency(0)
                      ->seal();
    std::vector<s4u::LinkInRoute> pci_cable_up(
        { s4u::LinkInRoute(pciNotFat, s4u::LinkInRoute::Direction::UP), s4u::LinkInRoute(pciFat) });
    std::vector<s4u::LinkInRoute> pci_cable_down(
        { s4u::LinkInRoute(pciNotFat, s4u::LinkInRoute::Direction::DOWN), s4u::LinkInRoute(pciFat) });

    // create CPUs
    for (int i = 0; i < num_cpus; i++) {
        s4u::Host *cpu = host_zone->create_host(hostname + "_CPU" + std::to_string(i), cpu_speed)->seal();
        host_zone->add_route(cpu->get_netpoint(), nic->get_netpoint(), nullptr, nullptr, pci_cable_up, false);
        host_zone->add_route(nic->get_netpoint(), cpu->get_netpoint(), nullptr, nullptr, pci_cable_down, false);
    }

    return std::make_pair(host_zone->get_netpoint(), nic->get_netpoint());
}

extern "C" {
void load_platform()
{
    s4u::create_fatTree_zone("cluster", nullptr, { 1, { 32 }, { 1 }, { 1 } }, { create_hostzone, {}, {} }, bxi_bw,
                             bxi_lat, s4u::Link::SharingPolicy::SPLITDUPLEX)
        ->seal();
}

void configure_engine(const s4u::Engine &e)
{
    // e.set_config("smpi/"
    //              "os:0:2.2617745265427535e-06:2.703352029721588e-10;392:2.8385604119907807e-06:1.1264760290330328e-10;"
    //              "32712:0.0:0.0;186836:0.0:0.0;1538469:0.0:0.0");
    // e.set_config("smpi/"
    //              "or:0:4.879281137093516e-07:2.2618415203462987e-10;392:5.779857092751072e-07:3.8094447278873e-11;"
    //              "32712:0.0:0.0;186836:0.0:0.0;1538469:0.0:0.0");
    // e.set_config("smpi/"
    //              "ois:0:5.673519212129521e-07:7.174648137343064e-43;392:5.446695252027938e-07:1.9686168165027744e-11;"
    //              "32712:5.536713106693264e-07:5.082661224033066e-13;186836:5.49289659548938e-07:3.189291810074661e-14;"
    //              "1538469:4.306002121584747e-07:4.21325930847998e-14");
    // e.set_config("smpi/"
    //              "bw-factor:0:0.2699228069054274;392:0.7647794357220394;32712:0.9664149337828888;186836:0."
    //              "9243880418172589;1538469:0.9732858005841204");
    // e.set_config("smpi/"
    //              "lat-factor:0:2.7676182391675814;392:3.9432860784637396;32712:10.117401654450653;186836:7."
    //              "810638891189083;1538469:18.84747638996546");
    // e.set_config("smpi/async-small-thresh:32712");
    // e.set_config("smpi/send-is-detached-thresh:32712");
    // e.set_config("smpi/iprobe:1.9586518031175638e-07");
    // e.set_config("smpi/test:1.1523121197560569e-07");
}
}